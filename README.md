# Updates...
Ok, I got a bunch of emails asking about support and further developmetn of this b%llshit. The answer is: It will no longer be activly developed and no attempts will be made to intergate in into FreeBSD ecosystem. The story how this code appeared is simple: I was building a custom OpenBSD kernel and implemlemented a sandbox for it as there is no Jails subsystem in OpenBSD.

## Why the development was dropped?
Because this code provides an illusion of security. Also, during the development of this kernel module I found out many bad aspects about FreeBSD kernel modules.
- this kernel module is useless when workign with File Sytsem. The FreeBSD's VNODE and namecache systems are not suitable for this. When you try to convert VNODE to FS-path, it usually fails or takes so many CPU time, that no one will ever use it in production.
- this is an illusion of secirity. Well it will cause some problems to attacker to break a sandboxed program, but due to known issues with FS-path coversion and other potential kernel vulerenabilities, it has no sense. As far as I know, the "sandbox" which Apple use in OSX and iOS is not too successfull. So, why bother?
- PERFORMANCE !!! when I was working on this, there was no Spectre and Meltdown vulns. So, by using some simple arithmetics, you will easily find out that the overhead for calling most syscalls will be horrible. So, why bother?
- There is so many code to check and so many exceptions left which are not covered (I mean, some mac framework functions can provide NULL-pointer as argument in some cases).
- Mac Framework is good, but deeper integration is required, I don;t have time. In my opinion, it will be bettwer to use hardware virtualization systems, which is now available in most CPU, and isolate the processes in the virtual environment, rather that have another stick in the kernel wheel.

## How to fix this?
Maybe if one day I become reach and there will be no need to work anymore, I will fix everything ))) kek.
What is required to be fixed:
- vn_fullpath(9) this function is garbadge. It requires a lot of CPU time and fails too often. In my opinion, there is no need to retrive an human readable path. The function could return an ID or INODE of each file or dir (on something which can be compares in one cycle). The FS-paths in the sandbox scheme could be converted from human readable to FS-path.
- If it is not possible to fix VNODE subsystem, then integrate the sandbox deeper into the kernel and provide the sandbox with raw data from the kernel syscall wrappers (I mean before the FS-path is converted to VNODE)
- a fast hashing algorithm is required to match sysctl and FS-path. It must be collision-safe.
- Regex library is not fast enough.
- Optimize the padding in the sandbox scheme.
- Rewrite userland in Rust (Rust will solve problem with "bad memory managment")
- Rewrite kernel module in Rust (C is better than Rust, but Rust solves many problems)
- The sandbox still can be bypassed by unloading it, so the deeper integration is NEEDED!

## What was not implemented?
- libsniffbox. This is a library which is preloaded to intercept a call to the heap managment system. The idea was to detect a malicious activity with heap, but not in real time. It was planned that it will send the data to remote server or local where it would be analyzed. Forget about it. I measured the performance and it is horrible and it may disclosure the content of the heap.
- priviledge escalaction and malicious activity. I noticed that potentially in some cases it is possible to detect such situation. But I have not tested it.

## What is the problem?
The illusion of the security. This software was just an experiment. And I can say that is was 80% successfull and it can prevent priviledged process from most things.

## Plannnnnns:
In nearest future: 2, 3 month I am planning to release some documentaion on this crap and add comments to the code.
In the long perspective: I am planning to redevelop the userland software, so it would be written in Rust.
What else? Datz it.

## How can I help.
This repository in my personal sandbox and no one is allowed to play with in it, but you can fork it and do whatever you want!!! Please, don;t bother a lot. Due to the problems with virus which is all around the world, I am too busy.

## Thoughts:
The sandbox covers almost everything. From sysctl to network and other. I thought about how to deversify the sandbox and came up to the following solution, for example:
- most of the programs don't need access to the most Sysctl which are available in the system. So, why not to integrate into the sysstl a verificator which will allow access only to the certain sysctl even if process is running as root.

So the same story with other subsystems. The MAC Framework does not allow to do many things.

# Sandbox security kernel module for FreeBSD
A sandbox is a kernel module which is based on the Trusted BSD MAC Framework. THe main function is to apply additional constraints 
on the running application in order to protect the integrity of the system and keep it secure.

ATTENTION! this is a development snapshot, and it will NOT compile because at the moment it is under active development!
This is only a backup commit.

# Info
The project page is located at [Project Page/Company Website](https://www.relkom.sk/en/fbsd_sandbox.shtml)  
The project and both website is under development. The documentation will be available later.  

